<?php

namespace App\Tests;
use App\Entity\DateDispo;
use App\Entity\Location;
use App\Entity\Thematique;
use App\Entity\Category;

use App\Tests\Unit\Tools\AssertEntityTrait;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DateDispoTest extends KernelTestCase
{

    use AssertEntityTrait; 

    public function setUp(): void
    {
        $this->category = (new Category())
            ->setLibelle("maison");
        
        $this->thematique = (new Thematique())
            ->setLibelle("amour");

        $this->location = (new Location())
            ->setLibelle("tis is the libelle")
            ->setTitle("tis is the title")
            ->setMaxPerson(2)
            ->setMaxPerson(5)
            ->setRoom(5)
            ->setMinPrice(50.12)
            ->setDescription("zhhzvuebvbubeizuvuhebfvufhefuyvbehfvbhudfyb j uegyrouzbjvhbsucvjsvuhe")
            ->setNumber(12)
            ->setStreet("polydor")
            ->setAdditionalAdress("chez moi")
            ->setCity("paris")
            ->setPostalCode("75015")
            ->setCountry("France")
            ->setDateLocation(new \DateTimeImmutable())
            ->setCategory($this->category)
            ->setThematiques($this->thematique);

        $this->dateDispo = (new DateDispo())
            ->setSeason("Hiver")
            ->setSeason("maison")
            ->setDateEnd(new \DateTimeImmutable())
            ->setLocation($this->location);
    }

    public function testValideDateDispo(): void
    {
        $this->assertHasErrors($this->dateDispo , 0);
    }

    public function testSeasonBlankUser(): void {
        $this->assertHasErrors($this->dateDispo->setSeason(""), 1, 'season', 'This value should not be blank.');
    }
}
