<?php

namespace App\Tests;
use App\Entity\User;
use App\Tests\Unit\Tools\AssertEntityTrait;


use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserTest extends KernelTestCase
{

    use AssertEntityTrait; 

    public function setUp(): void
    { 
        $this->user = (new User())
            ->setEmail("a@g.com")
            ->setRoles(["ROLE_USER'"])
            ->setPassword("azertyyu")
            ->setName("DUPON")
            ->setBirthDate(new \DateTimeImmutable())
            ->setGender("Male")
            ->setPhoneNumber("0665234578")
            ->setFirstname("DUPON");
       
    }

    public function testValideFilterSearch(): void
    {
        $this->assertHasErrors($this->user , 0);
    }

    public function testEmailBlankUser(): void {
        $this->assertHasErrors($this->user->setEmail(""), 2, 'email', 'This value should not be blank.');
        $this->assertHasErrors($this->user->setEmail(""), 2, 'email', 'This value is too short. It should have 6 characters or more.');
    }

    public function testEmailTooLongUser(): void {
        $this->assertHasErrors($this->user->setEmail("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa@ggggggggggggggggggggggggggggggggggggggggggggg.com"), 1, 'email', 'This value is too long. It should have 180 characters or less.');
    }

    public function testEmailNotValideUser(): void {
        $this->assertHasErrors($this->user->setEmail("::::::"), 1, 'email', 'This value is not valid.');
    }

    public function testRolesBlankUser(): void {
        $this->assertHasErrors($this->user->setRoles([]), 1, 'roles', 'This value should not be blank.');
    }

    public function testPasswordBlankUser(): void {
        $this->assertHasErrors($this->user->setPassword(""), 2, 'password', 'This value should not be blank.');
        $this->assertHasErrors($this->user->setPassword(""), 2, 'password', 'This value is too short. It should have 8 characters or more.');
    }

    public function testPasswordTooLongUser(): void {
        $this->assertHasErrors($this->user->setPassword("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), 1, 'password', 'This value is too long. It should have 255 characters or less.');
    }

    public function testPasswordTooShortUser(): void {
        $this->assertHasErrors($this->user->setPassword("a"), 1, 'password', 'This value is too short. It should have 8 characters or more.');
    }

    public function testPasswordNotValideUser(): void {
        $this->assertHasErrors($this->user->setPassword(">>>>>>>>"), 1, 'password', 'This value is not valid.');
    }

    public function testNameBlankUser(): void {
        $this->assertHasErrors($this->user->setName(""), 1, 'name', 'This value is too short. It should have 1 character or more.');
    }

    public function testNameTooLongUser(): void {
        $this->assertHasErrors($this->user->setName("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), 1, 'name', 'This value is too long. It should have 255 characters or less.');
    }

    public function testNameTooShortUser(): void {
        $this->assertHasErrors($this->user->setName(""), 1, 'name', 'This value is too short. It should have 1 character or more.');
    }

    public function testNameNotValideUser(): void {
        $this->assertHasErrors($this->user->setName(">>>>"), 1, 'name', 'This value is not valid.');
    }

    public function testFirstTooLongUser(): void {
        $this->assertHasErrors($this->user->setFirstName("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), 1, 'firstname', 'This value is too long. It should have 255 characters or less.');
    }

    public function testFirstnameNotValideUser(): void {
        $this->assertHasErrors($this->user->setFirstName(">>>>>>"), 1, 'firstname', 'This value is not valid.');
    }

    public function testGenderBlankUser(): void {
        $this->assertHasErrors($this->user->setGender(""), 2, 'gender', 'This value should not be blank.');
        $this->assertHasErrors($this->user->setGender(""), 2, 'gender', 'This value is too short. It should have 1 character or more.');
    }

    public function testGenderTooLongUser(): void {
        $this->assertHasErrors($this->user->setGender("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), 1, 'gender', 'This value is too long. It should have 255 characters or less.');
    }

    public function testGenderNotValideUser(): void {
        $this->assertHasErrors($this->user->setGender(">"), 1, 'gender', 'This value is not valid.');
    }

    public function testPhoneNumberNotValideUser(): void {
        $this->assertHasErrors($this->user->setPhoneNumber("addd"), 1, 'phoneNumber', 'This value is not valid.');
    }
    
}
