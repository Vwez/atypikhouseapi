<?php

namespace App\Tests;
use App\Entity\Thematique;
use Faker\Provider\Lorem;
use App\Tests\Unit\Tools\AssertEntityTrait;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ThematiqueTest extends KernelTestCase
{
    use AssertEntityTrait; 
    public function setUp(): void
    { 
        $this->thematique = (new Thematique())
            ->setLibelle("mason en bois");
       
    }

    public function testValideThematique(): void
    {
       $this->assertHasErrors($this->thematique , 0);

    }

    public function testLibelleBlankThematique(): void {
        $this->assertHasErrors($this->thematique->setLibelle(""), 2, 'libelle', 'This value should not be blank.');
        $this->assertHasErrors($this->thematique->setLibelle(""), 2, 'libelle', 'This value is too short. It should have 4 characters or more.');
    }

    public function testLibelleTooLongThematique(): void {
        $this->assertHasErrors($this->thematique->setLibelle("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), 1, 'libelle', 'This value is too long. It should have 255 characters or less.');
    }

    public function testLibelleTooShortThematique(): void {
        $this->assertHasErrors($this->thematique->setLibelle("a"), 1, 'libelle', 'This value is too short. It should have 4 characters or more.');
    }

    public function testLibelleNotValideThematique(): void {
        $this->assertHasErrors($this->thematique->setLibelle(">>>>"), 1, 'libelle', 'This value is not valid.');
    }
}
