<?php

namespace App\Tests;
use App\Entity\Adress;
use Faker\Provider\Lorem;
use App\Tests\Unit\Tools\AssertEntityTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


class AdressTest extends KernelTestCase
{
    use AssertEntityTrait; 

    public function setUp(): void
    { 
        $this->adress = (new Adress())
            ->setStreet("la rue")
            ->setCity("la ville")
            ->setCountry("le pays")
            ->setAdditionalAdress("complement d'adresse");
            //->setLocation("location")&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
            //->setUser("utilisateur);
           
    }

    public function testValideAdress(): void
    {
       $this->assertHasErrors($this->adress , 0);

    }
    
    // Test street
    public function testStreetBlankAdress(): void {
        $this->assertHasErrors($this->adress->setStreet(""), 2, 'street', 'This value should not be blank.');
        $this->assertHasErrors($this->adress->setStreet(""), 2, 'street', 'This value is too short. It should have 4 characters or more.');
    }

    public function testStreetTooLongAdress(): void {
        $this->assertHasErrors($this->adress->setStreet("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), 1, 'street', 'This value is too long. It should have 255 characters or less.');
       
    }

    public function testStreetTooShortAdress(): void {
        $this->assertHasErrors($this->adress->setStreet("a"), 1, 'street', 'This value is too short. It should have 4 characters or more.');
    }

    public function testStreetNotValideAdress(): void {
        $this->assertHasErrors($this->adress->setStreet(">>>>"), 1, 'street', 'This value is not valid.');
    }

      //Test city
    public function testCityTooLongAdress(): void {
        $this->assertHasErrors($this->adress->setCity("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), 1, 'city', 'This value is too long. It should have 255 characters or less.');
       
    }

    public function testCityTooShortAdress(): void {
        $this->assertHasErrors($this->adress->setCity("a"), 1, 'city', 'This value is too short. It should have 4 characters or more.');
    }

    public function testCityNotValideAdress(): void {
        $this->assertHasErrors($this->adress->setCity(">>>>"), 1, 'city', 'This value is not valid.');
    }

        //Test country
    public function testCountryTooLongAdress(): void {
        $this->assertHasErrors($this->adress->setCountry("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), 1, 'country', 'This value is too long. It should have 255 characters or less.');
       
    }

    
    public function testCountryTooShortAdress(): void {
        $this->assertHasErrors($this->adress->setCountry("a"), 1, 'country', 'This value is too short. It should have 4 characters or more.');
    }

    public function testCountryNotValideAdress(): void {
        $this->assertHasErrors($this->adress->setCountry(">>>>"), 1, 'country', 'This value is not valid.');
    }

     //Test additionalAdress
     public function testAdditionalAdressTooLongAdress(): void {
        $this->assertHasErrors($this->adress->setAdditionalAdress("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), 1, 'additionalAdress', 'This value is too long. It should have 255 characters or less.');
       
    }

    
    public function testAdditionalAdressTooShortAdress(): void {
        $this->assertHasErrors($this->adress->setAdditionalAdress("a"), 1, 'additionalAdress', 'This value is too short. It should have 4 characters or more.');
    }

    public function testAdditionalAdressNotValideAdress(): void {
        $this->assertHasErrors($this->adress->setAdditionalAdress(">>>>"), 1, 'additionalAdress', 'This value is not valid.');
    }


}
