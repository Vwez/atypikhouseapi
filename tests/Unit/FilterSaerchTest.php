<?php

namespace App\Tests;
use App\Entity\FilterSearch;
use App\Tests\Unit\Tools\AssertEntityTrait;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class FilterSaerchTest extends KernelTestCase
{
    use AssertEntityTrait; 

    public function setUp(): void
    { 
        $this->filterSearch = (new FilterSearch())
            ->setCity("kito")
            ->setCategory("roulote")
            ->setNbPerson(5)
            ->setDate(new \DateTimeImmutable());
       
    }

    public function testValideFilterSearch(): void
    {
        $this->assertHasErrors($this->filterSearch , 0);
    }

    public function testCityBlankFilterSearch(): void {
        $this->assertHasErrors($this->filterSearch->setCity(""), 2, 'city', 'This value should not be blank.');
        $this->assertHasErrors($this->filterSearch->setCity(""), 2, 'city', 'This value is too short. It should have 1 character or more.');
    }

    public function testCityTooLongFilterSearch(): void {
        $this->assertHasErrors($this->filterSearch->setCity("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), 1, 'city', 'This value is too long. It should have 255 characters or less.');
    }

    public function testCityNotValideFilterSearch(): void {
        $this->assertHasErrors($this->filterSearch->setCity(">"), 1, 'city', 'This value is not valid.');
    }

    public function testCategoryBlankFilterSearch(): void {
        $this->assertHasErrors($this->filterSearch->setCategory(""), 2, 'category', 'This value should not be blank.');
        $this->assertHasErrors($this->filterSearch->setCategory(""), 2, 'category', 'This value is too short. It should have 4 characters or more.');
    }

    public function testCategoryTooLongFilterSearch(): void {
        $this->assertHasErrors($this->filterSearch->setCategory("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), 1, 'category', 'This value is too long. It should have 255 characters or less.');
    }

    public function testCategoryNotValideFilterSearch(): void {
        $this->assertHasErrors($this->filterSearch->setCity(">>>>"), 1, 'city', 'This value is not valid.');
    }

    public function testNbPersonNotValideFilterSearch(): void {
        $this->assertHasErrors($this->filterSearch->setNbPerson(10.3), 1, 'nbPerson', 'This value is not valid.');
    }

    

}
