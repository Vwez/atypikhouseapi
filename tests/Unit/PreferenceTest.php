<?php

namespace App\Tests;
use App\Entity\Preference;
use Faker\Provider\Lorem;
use App\Tests\Unit\Tools\AssertEntityTrait;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PreferenceTest extends KernelTestCase
{
    use AssertEntityTrait; 

    public function setUp(): void
    { 
        $this->preference = (new Preference())
            ->setChoice1("mon choix 1")
            ->setChoice2("mon choix 2")
            ->setChoice3("mon choix 3");
            
    }

    public function testValidePreference(): void
    {
       $this->assertHasErrors($this->preference , 0);

    }
    
    // Test choice1
   public function testChoice1TooLongPreference(): void {
        $this->assertHasErrors($this->preference->setChoice1("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), 1, 'choice1', 'This value is too long. It should have 255 characters or less.');
       
    }

    public function testChoice1TooShortPreference(): void {
        $this->assertHasErrors($this->preference->setChoice1("a"), 1, 'choice1', 'This value is too short. It should have 4 characters or more.');
    }

    public function testChoice1NotValidePreference(): void {
        $this->assertHasErrors($this->preference->setChoice1(">>>>"), 1, 'choice1', 'This value is not valid.');
    }

    // Test choice2
    public function testChoice2TooLongPreference(): void {
        $this->assertHasErrors($this->preference->setChoice2("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), 1, 'choice2', 'This value is too long. It should have 255 characters or less.');
    }

    public function testChoice2TooShortPreference(): void {
        $this->assertHasErrors($this->preference->setChoice2("a"), 1, 'choice2', 'This value is too short. It should have 4 characters or more.');
    }

    public function testChoice2NotValidePreference(): void {
        $this->assertHasErrors($this->preference->setChoice2(">>>>"), 1, 'choice2', 'This value is not valid.');
    }

    //Test choice3
    public function testChoice3TooLongPreference(): void {
        $this->assertHasErrors($this->preference->setChoice3("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), 1, 'choice3', 'This value is too long. It should have 255 characters or less.');
    }

    public function testChoice3TooShortPreference(): void {
        $this->assertHasErrors($this->preference->setChoice3("a"), 1, 'choice3', 'This value is too short. It should have 4 characters or more.');
    }

    public function testChoice3NotValidePreference(): void {
        $this->assertHasErrors($this->preference->setChoice3(">>>>"), 1, 'choice3', 'This value is not valid.');
    }





}
