<?php

namespace App\Tests;
use App\Entity\Reservation;
use App\Entity\User;
use App\Entity\Location;
use App\Entity\Thematique;
use App\Entity\Category;
use App\Tests\Unit\Tools\AssertEntityTrait;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ReservationTest extends KernelTestCase
{

    use AssertEntityTrait; 
    public function setUp(): void
    { 
        $this->category = (new Category())
            ->setLibelle("maison");
        
        $this->thematique = (new Thematique())
            ->setLibelle("amour");

        $this->user = (new User())
            ->setEmail("a@g.com")
            ->setRoles(["ROLE_USER'"])
            ->setPassword("azertyyu")
            ->setName("DUPON")
            ->setBirthDate(new \DateTimeImmutable())
            ->setGender("Male")
            ->setPhoneNumber("0665234578")
            ->setFirstname("DUPON");

        // $this->comment = (new Comment())
        //     ->setTitle("tis is the title")
        //     ->setContent("zbhbvhbsdvbvbuegvhevgfebvhjbeyvgyevbuyvbevyrzvezgyvbhbvuyevebvbhjezbyrvbervjheb")
        //     ->setCreatedAt(new \DateTimeImmutable())
        //     ->setRating(5)
        //     ->setNbrOfResponse(9)
        //     ->setUpdatedAt(new \DateTimeImmutable())
        //     ->setDeletedAt(new \DateTimeImmutable());
        
        $this->location = (new Location())
            ->setLibelle("tis is the libelle")
            ->setTitle("tis is the title")
            ->setMaxPerson(2)
            ->setMaxPerson(5)
            ->setRoom(5)
            ->setMinPrice(50.12)
            ->setDescription("zhhzvuebvbubeizuvuhebfvufhefuyvbehfvbhudfyb j uegyrouzbjvhbsucvjsvuhe")
            ->setNumber(12)
            ->setStreet("polydor")
            ->setAdditionalAdress("chez moi")
            ->setCity("paris")
            ->setPostalCode("75015")
            ->setCountry("France")
            ->setDateLocation(new \DateTimeImmutable())
            ->setCategory($this->category)
            ->setThematiques($this->thematique);

        $this->reservation = (new Reservation())
            ->setTtcPrice(123.15)
            ->setBookingStartDate(new \DateTimeImmutable())
            ->setBookingEndDate(new \DateTimeImmutable())
            ->setBookingDate(new \DateTimeImmutable())
            ->setLocation($this->location)
            ->setReservationState('validated')
            ->setUser($this->user);
       
    }

    public function testValideFilterSearch(): void
    {
        $this->assertHasErrors($this->reservation , 0);
    }


}
