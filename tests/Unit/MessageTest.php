<?php

namespace App\Tests;
use App\Entity\Message;
use App\Tests\Unit\Tools\AssertEntityTrait;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class MessageTest extends KernelTestCase
{
    use AssertEntityTrait; 

    public function setUp(): void
    { 
        $this->message = (new Message())
            ->setName("Essooussoue")
            ->setFirstName("Jean")
            ->setContent("rurjfjfjsfnjvjjdjfufjhsdfncshshdqdqjhdjkkcsndfjfufjhfhgtigtikfjdjduhdehjddjjddjdjdjdjdjdjdjdjdsjsjjjsdqhchqcnjkqjkhcqhkjcqcqchqkjsssssssssssssssssssssssssssssssssssqqqqqqqqqqqqqqqqqkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkqiaqqqqqqqqqqqqqqqqqqqqsazazmmmmmm")
            ->setSubject("question")
            ->setForwarderMail("63@gmail.com")
            ->setCreateAt(new \DateTimeImmutable())
            ->setTimeCreate(new \DateTimeImmutable());
       
    }

    public function testValideMessage(): void
    {
        $this->assertHasErrors($this->message , 0);
    }

    // name

    public function testNameBlankMessage(): void {
        $this->assertHasErrors($this->message->setName(""), 2, 'name', 'This value should not be blank.');
        $this->assertHasErrors($this->message->setName(""), 2, 'name', 'This value is too short. It should have 1 character or more.');
    }

    public function testNameTooLongMessager(): void {
        $this->assertHasErrors($this->message->setName("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), 1, 'name', 'This value is too long. It should have 255 characters or less.');
    }

    public function testNameTooShortMessage(): void {
        $this->assertHasErrors($this->message->setName(""), 2, 'name', 'This value is too short. It should have 1 character or more.');
    }

    public function testNameNotValideMessage(): void {
        $this->assertHasErrors($this->message->setName(">>>>"), 1, 'name', 'This value is not valid.');
    }

    // firstName    

    public function testFirstTooLongMessage(): void {
        $this->assertHasErrors($this->message->setFirstName("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), 1, 'firstName', 'This value is too long. It should have 255 characters or less.');
    }

    public function testFirstnameNotValideMessage(): void {
        $this->assertHasErrors($this->message->setFirstName(">>>>>>"), 1, 'firstName', 'This value is not valid.');
    }

    // subject
    public function testSubjectBlankMessage(): void {
        $this->assertHasErrors($this->message->setSubject(""), 2, 'subject', 'This value should not be blank.');
        $this->assertHasErrors($this->message->setSubject(""), 2, 'subject', 'This value is too short. It should have 5 characters or more.');
    }

    public function testSubjectTooLongMessager(): void {
        $this->assertHasErrors($this->message->setSubject("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), 1, 'subject', 'This value is too long. It should have 255 characters or less.');
    }

    public function testSubjectTooShortMessage(): void {
        $this->assertHasErrors($this->message->setSubject(""), 2, 'subject', 'This value is too short. It should have 5 characters or more.');
    }

    public function testSubjectNotValideMessage(): void {
        $this->assertHasErrors($this->message->setSubject(">>>>4"), 1, 'subject', 'This value is not valid.');
    }

    // content 
    public function testcontentBlankMessage(): void {
        $this->assertHasErrors($this->message->setContent(""), 2, 'content', 'This value should not be blank.');
        $this->assertHasErrors($this->message->setContent(""), 2, 'content', 'This value is too short. It should have 1 character or more.');
    }


    public function testContentTooShortMessage(): void {
        $this->assertHasErrors($this->message->setContent(""), 2, 'content', 'This value is too short. It should have 1 character or more.');
    }

    // forwarderMail

    public function testForwarderMailBlankUser(): void {
        $this->assertHasErrors($this->message->setForwarderMail(""), 2, 'forwarderMail', 'This value should not be blank.');
        $this->assertHasErrors($this->message->setForwarderMail(""), 2, 'forwarderMail', 'This value is too short. It should have 6 characters or more.');
    }

    public function testForwarderMailTooLongUser(): void {
        $this->assertHasErrors($this->message->setForwarderMail("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa@ggggggggggggggggggggggggggggggggggggggggggggg.com"), 1, 'forwarderMail', 'This value is too long. It should have 70 characters or less.');
    }

    public function testForwarderMailNotValideUser(): void {
        $this->assertHasErrors($this->message->setForwarderMail("::::::"), 1, 'forwarderMail', 'This value is not valid.');
    }



}
