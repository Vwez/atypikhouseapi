<?php

namespace App\Tests;
use App\Entity\Comment;
use App\Entity\Location;
use App\Entity\Reservation;
use App\Entity\User;
use App\Entity\Category;
use App\Entity\Thematique;


use App\Tests\Unit\Tools\AssertEntityTrait;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CommentTest extends KernelTestCase
{
    use AssertEntityTrait; 

    public function setUp(): void
    {
        $this->category = (new Category())
            ->setLibelle("maison");
        
        $this->thematique = (new Thematique())
            ->setLibelle("amour");

        $this->user = (new User())
            ->setEmail("a@g.com")
            ->setRoles(["ROLE_USER'"])
            ->setPassword("azertyyu")
            ->setName("DUPON")
            ->setBirthDate(new \DateTimeImmutable())
            ->setGender("Male")
            ->setPhoneNumber("0665234578")
            ->setFirstname("DUPON");

            $this->location = (new Location())
            ->setLibelle("tis is the libelle")
            ->setTitle("tis is the title")
            ->setMaxPerson(2)
            ->setMaxPerson(5)
            ->setRoom(5)
            ->setMinPrice(50.12)
            ->setDescription("zhhzvuebvbubeizuvuhebfvufhefuyvbehfvbhudfyb j uegyrouzbjvhbsucvjsvuhe")
            ->setNumber(12)
            ->setStreet("polydor")
            ->setAdditionalAdress("chez moi")
            ->setCity("paris")
            ->setPostalCode("75015")
            ->setCountry("France")
            ->setDateLocation(new \DateTimeImmutable())
            ->setCategory($this->category)
            ->setThematiques($this->thematique);

        $this->reservation = (new Reservation())
            ->setTtcPrice(123.15)
            ->setBookingStartDate(new \DateTimeImmutable())
            ->setBookingEndDate(new \DateTimeImmutable())
            ->setBookingDate(new \DateTimeImmutable())
            ->setLocation($this->location)
            ->setReservationState('validated')
            ->setUser($this->user);

        $this->comment = (new Comment())
            ->setTitle("tis is the title")
            ->setContent("zbhbvhbsdvbvbuegvhevgfebvhjbeyvgyevbuyvbevyrzvezgyvbhbvuyevebvbhjezbyrvbervjheb")
            ->setCreatedAt(new \DateTimeImmutable())
            ->setRating(5)
            ->setNbrOfResponse(9)
            ->setReservation($this->reservation)
            ->setUpdatedAt(new \DateTimeImmutable())
            ->setDeletedAt(new \DateTimeImmutable());
    }

    public function testValideComment(): void
    {
        $this->assertHasErrors($this->comment , 0);
    }

    public function testTitleBlankComment(): void {
        $this->assertHasErrors($this->comment->setTitle(""), 2, 'title', 'This value should not be blank.');
        $this->assertHasErrors($this->comment->setTitle(""), 2, 'title', 'This value is too short. It should have 5 characters or more.');
    }

    public function testTitleTooLongComment(): void {
        $this->assertHasErrors($this->comment->setTitle("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), 1, 'title', 'This value is too long. It should have 255 characters or less.');
    }

    public function testTitleTooShortComment(): void {
        $this->assertHasErrors($this->comment->setTitle(""), 2, 'title', 'This value is too short. It should have 5 characters or more.');
    }

    public function testTitleNotValideComment(): void {
        $this->assertHasErrors($this->comment->setTitle(">>>>>>"), 1, 'title', 'This value is not valid.');
    }

    public function testContentBlankcomment(): void {
        $this->assertHasErrors($this->comment->setContent(""), 2, 'content', 'This value should not be blank.');
        $this->assertHasErrors($this->comment->setContent(""), 2, 'content', 'This value is too short. It should have 1 character or more.');
    }

    public function testRatingComment(): void {
        $this->assertHasErrors($this->comment->setRating(10.3), 1, 'rating', 'This value is not valid.');
    }

    public function testNbrOfResponseComment(): void {
        $this->assertHasErrors($this->comment->setNbrOfResponse(10.3), 1, 'nbrOfResponse', 'This value is not valid.');
    }
}
