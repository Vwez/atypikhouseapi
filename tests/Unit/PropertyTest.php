<?php

namespace App\Tests;
use App\Entity\Property;
use Faker\Provider\Lorem;
use App\Tests\Unit\Tools\AssertEntityTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PropertyTest extends KernelTestCase
{
    use AssertEntityTrait; 

    public function setUp(): void
    { 
        $this->property = (new Property())
            ->setTitle("le titre")
            ->setDescription("la description") ;
           // ->setCategory("location") ;
           // ->setPvalues;
            
    }

    public function testValideProperty(): void
    {
       $this->assertHasErrors($this->property , 0);

    }
    
    // Test title
    public function testTitleBlankProperty(): void {
        $this->assertHasErrors($this->property->setTitle(""), 2, 'title', 'This value should not be blank.');
        $this->assertHasErrors($this->property->setTitle(""), 2, 'title', 'This value is too short. It should have 4 characters or more.');
    }

    public function testTitleTooLongProperty(): void {
        $this->assertHasErrors($this->property->setTitle("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), 1, 'title', 'This value is too long. It should have 255 characters or less.');
       
    }

    public function testTitleTooShortProperty(): void {
        $this->assertHasErrors($this->property->setTitle("a"), 1, 'title', 'This value is too short. It should have 4 characters or more.');
    }

    public function testTitleNotValideProperty(): void {
        $this->assertHasErrors($this->property->setTitle(">>>>"), 1, 'title', 'This value is not valid.');
    }

    //Test description
    public function testDescriptionTooLongProperty(): void {
        $this->assertHasErrors($this->property->setDescription("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), 1, 'description', 'This value is too long. It should have 255 characters or less.');
    }

    public function testDescriptionTooShortProperty(): void {
        $this->assertHasErrors($this->property->setDescription("a"), 1, 'description', 'This value is too short. It should have 4 characters or more.');
    }

    public function testDescriptionNotValidePropertyt(): void {
        $this->assertHasErrors($this->property->setDescription(">>>>"), 1, 'description', 'This value is not valid.');
    }

    //Test category

}
