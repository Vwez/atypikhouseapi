<?php

namespace App\Tests;
use App\Entity\Category;
use Faker\Provider\Lorem;
use App\Tests\Unit\Tools\AssertEntityTrait;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CategoryTest extends KernelTestCase
{

    use AssertEntityTrait; 
    public function setUp(): void
    { 
        $this->category = (new Category())
            ->setLibelle("cabane");
       
    }

    public function testValideCategory(): void
    {
       $this->assertHasErrors($this->category , 0);

    }

    public function testLibelleBlankCategory(): void {
        $this->assertHasErrors($this->category->setLibelle(""), 2, 'libelle', 'This value should not be blank.');
        $this->assertHasErrors($this->category->setLibelle(""), 2, 'libelle', 'This value is too short. It should have 4 characters or more.');
    }

    public function testLibelleTooLongCategory(): void {
        $this->assertHasErrors($this->category->setLibelle("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), 1, 'libelle', 'This value is too long. It should have 255 characters or less.');
    }

    public function testLibelleTooShortCategory(): void {
        $this->assertHasErrors($this->category->setLibelle("a"), 1, 'libelle', 'This value is too short. It should have 4 characters or more.');
    }

    public function testLibelleNotValideCategory(): void {
        $this->assertHasErrors($this->category->setLibelle(">>>>"), 1, 'libelle', 'This value is not valid.');
    }
}
