<?php

namespace App\Tests;
use App\Entity\Activity;
use Faker\Provider\Lorem;
use App\Tests\Unit\Tools\AssertEntityTrait;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ActivityTest extends KernelTestCase
{
    use AssertEntityTrait; 

    public function setUp(): void
    { 
        $this->activity = (new Activity())
            ->setTitle("le titre")
            ->setType("le type") ;
           // ->setLocations("location") ;
            
    }

    public function testValideActivity(): void
    {
       $this->assertHasErrors($this->activity , 0);

    }
    
    // Test title
    public function testTitleBlankActivity(): void {
        $this->assertHasErrors($this->activity->setTitle(""), 2, 'title', 'This value should not be blank.');
        $this->assertHasErrors($this->activity->setTitle(""), 2, 'title', 'This value is too short. It should have 4 characters or more.');
    }

    public function testTitleTooLongActivity(): void {
        $this->assertHasErrors($this->activity->setTitle("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), 1, 'title', 'This value is too long. It should have 255 characters or less.');
       
    }

    public function testTitleTooShortActivity(): void {
        $this->assertHasErrors($this->activity->setTitle("a"), 1, 'title', 'This value is too short. It should have 4 characters or more.');
    }

    public function testTitleNotValideActivity(): void {
        $this->assertHasErrors($this->activity->setTitle(">>>>"), 1, 'title', 'This value is not valid.');
    }

     // Test type
     public function testTypeBlankActivity(): void {
        $this->assertHasErrors($this->activity->setType(""), 2, 'type', 'This value should not be blank.');
        $this->assertHasErrors($this->activity->setType(""), 2, 'type', 'This value is too short. It should have 4 characters or more.');
    }

    public function testTypeTooLongActivity(): void {
        $this->assertHasErrors($this->activity->setType("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), 1, 'type', 'This value is too long. It should have 255 characters or less.');
       
    }

    public function testTypeTooShortActivity(): void {
        $this->assertHasErrors($this->activity->setType("a"), 1, 'type', 'This value is too short. It should have 4 characters or more.');
    }

    public function testTypeNotValideActivity(): void {
        $this->assertHasErrors($this->activity->setType(">>>>"), 1, 'type', 'This value is not valid.');
    }

    // Test Location

}
