<?php

namespace App\Tests;
use App\Entity\Equipement;
use Faker\Provider\Lorem;
use App\Tests\Unit\Tools\AssertEntityTrait;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class EquipementTest extends KernelTestCase
{
    use AssertEntityTrait; 

    //******** Test sur le libelle ************ */
    public function setUp(): void
    { 
        $this->equipement = (new Equipement())
            ->setLibelle("machine a laver")
            ->setDescription("Cette mchine dispose de plusieurs fonctions: lavage, sechage et rincage"); 
    }

    public function testValideEquipement(): void
    {
       $this->assertHasErrors($this->equipement , 0);

    }

    public function testLibelleBlankEquipement(): void {
        $this->assertHasErrors($this->equipement->setLibelle(""), 2, 'libelle', 'This value should not be blank.');
        $this->assertHasErrors($this->equipement->setLibelle(""), 2, 'libelle', 'This value is too short. It should have 4 characters or more.');
    }

   public function testLibelleTooLongEquipement(): void {
        $this->assertHasErrors($this->equipement->setLibelle("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), 1, 'libelle', 'This value is too long. It should have 255 characters or less.');
    }

    public function testLibelleTooShortEquipement(): void {
        $this->assertHasErrors($this->equipement->setLibelle("a"), 1, 'libelle', 'This value is too short. It should have 4 characters or more.');
    }

    public function testLibelleNotValideEquipement(): void {
        $this->assertHasErrors($this->equipement->setLibelle(">>>>"), 1, 'libelle', 'This value is not valid.');
    }


    //******** Test sur la description ************ *
   
    public function testDescriptionTooLongEquipement(): void {
        $this->assertHasErrors($this->equipement->setDescription("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), 1, 'description', 'This value is too long. It should have 255 characters or less.');
    }

    public function testDescriptionTooShortEquipement(): void {
        $this->assertHasErrors($this->equipement->setDescription("a"), 1, 'description', 'This value is too short. It should have 4 characters or more.');
    }

    public function testDescriptionNotValideEquipement(): void {
        $this->assertHasErrors($this->equipement->setDescription(">>>>"), 1, 'description', 'This value is not valid.');
    }

}
