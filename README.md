# AtypikHouseApi
The Company "AtypikHouse" is a web project done in order to validate our bashlor degree on web pogramming. 
It is done in order to build an API for  AtypikHouse with Symfony 6 and PHP 8.0

## Get the source code

This will download the source code of AtypikHouse:

```shell
git clone https://gitlab.com/Vwez/atypikhouseapi.git
cd atypikhouseapi
```

## Run the project

### step 1: check Technical Requirements

```shell
symfony check:requirements
```
### step 2: create database

```shell
## the commande will be displayed soon
```

### step 3: cionfigure the .env file tu set the database URL
For exemple:
```shell
DATABASE_URL="postgresql://symfony:ChangeMe@127.0.0.1:5432/app?serverVersion=13&charset=utf8"
```

### step 4: create the data base 

```shell
symfony console doctrine:database:create
```

### step 5: generate fake data

```shell
## the commande will be displayed soon
```

### step 6: run the project

Run the following commande for a dev server. Navigate to `http://localhost:8000/`. 
```shell
symfony server:start 
```



