<?php

namespace App\Repository;

use App\Entity\DateDispo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DateDispo|null find($id, $lockMode = null, $lockVersion = null)
 * @method DateDispo|null findOneBy(array $criteria, array $orderBy = null)
 * @method DateDispo[]    findAll()
 * @method DateDispo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DateDispoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DateDispo::class);
    }

    // /**
    //  * @return DateDispo[] Returns an array of DateDispo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DateDispo
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
