<?php

namespace App\DataFixtures;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Equipement;
// use App\Entity\User;
use Faker;

class EquipementFixtures extends Fixture implements DependentFixtureInterface
{

    public function getDependencies(){
        return [
            UserFixtures::class,
        ];
    }

    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Faker\Factory::create("fr_FR");
        for ($i=0; $i < 15; $i++) { 
            $equipement = new Equipement();
            $equipement->setLibelle($faker->word());
            $equipement->setDescription($faker->text(100));
            $equipement->setUser($this->getReference("USER".mt_rand(0, 3)));
            $manager->persist($equipement);
            $this->addReference("EQU".$i, $equipement);

        }

        $manager->flush();
    }
}
