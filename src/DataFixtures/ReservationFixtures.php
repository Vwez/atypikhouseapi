<?php

namespace App\DataFixtures;

use App\Entity\Reservation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ReservationFixtures extends Fixture implements DependentFixtureInterface
{

    public function getDependencies(){
        return [
            LocationFixtures::class,
            UserFixtures::class
        ];
    }

    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Faker\Factory::create("fr_FR");
        $state = ["wait-review", "validated","deleted"];

        for ($i=0; $i < 20; $i++) { 
            # code...$
            $reservation = new Reservation();
            $reservation->setTtcPrice($faker->numberBetween($min = 250, $max = 1000));
            $reservation->setBookingDate($faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now', $timezone = null));
            $reservation->setBookingStartDate($faker->dateTimeThisYear($max = 'now', $timezone = null));
            $reservation->setBookingEndDate($faker->dateTimeThisYear($max = 'now', $timezone = null));
            $reservation->setLocation($this->getReference("LOC".mt_rand(0, 29)));
            $reservation->setUser($this->getReference("USER".mt_rand(1, 3)));
            $reservation->setReservationState($state[mt_rand(0, 2)]);
            $manager->persist($reservation);
            $this->addReference("RES".$i, $reservation);
        }

        $manager->flush();
    }
}
