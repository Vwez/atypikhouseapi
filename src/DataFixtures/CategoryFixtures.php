<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $faker = Faker\Factory::create("fr_FR");
        // $faker = Factory::create("fr_FR");
        $categories =  array(
            "cabane dans les arbres", "bulle", "cabane sur l'eau", "cabane","cabana sur pilotis", "chalet",  
            "lov'nid", "roulotte", "cabane de trappeur","dôme", "maison troclodite", "maison de hobbit", "yourte",
            "tini house", "cabane verticale", "tipi", "bateau", "inclassable"
        );

        for ($i=0; $i < count($categories) ; $i++) { 
            $category = new Category();
            $category->setLibelle($categories[$i]);
            $manager->persist($category);
            $this->addReference("CAT".$i, $category);
        }

        $manager->flush();
    }
}
