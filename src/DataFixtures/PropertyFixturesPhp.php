<?php

namespace App\DataFixtures;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Property;
use Faker;


class PropertyFixturesPhp extends Fixture implements DependentFixtureInterface
{

    public function getDependencies(){
        return [
            CategoryFixtures::class,
        ];
    }

    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Faker\Factory::create("fr_FR");
        for ($i=0; $i < 10; $i++) { 
            $property = new Property();
            $property->setTitle($faker->text(50));
            $property->setDescription($faker->text(100));
            $property->setCategory($this->getReference("CAT".mt_rand(0, 17)));
            $manager->persist( $property);
            $this->addReference("PRO".$i, $property);
        }

        $manager->flush();
    }
}
