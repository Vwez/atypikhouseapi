<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;


class ActivityFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        // $categories =  array(
        //     "pêche", "chasse", "sky", "vélo","cabana sur pilotis", "randonnée",  
        //     "dégustation", "visite musées", "journée spa","Aller nager", "sport", "casino", "yourte",
        //     "tini house", "cabane verticale", "tipi", "bateau", "inclassable"
        // );

        $manager->flush();
    }
}
