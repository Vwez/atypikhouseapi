<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
// use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Faker;

class UserFixtures extends Fixture
{
    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Faker\Factory::create("fr_FR");
        $users = [
            "user@yopmail.com" => [],
            "owner1@yopmail.com" => ["ROLE_OWNER"],
            "owner2@yopmail.com" => ["ROLE_OWNER"],
            "admin@yopmail.com" => ["ROLE_ADMIN"],
        ];

        // creation users
        $genderTilte =  array("male", "female");
        $count = 0;

        foreach ($users as $email => $role) { 
            // $index = rand(0, 1);
            $index = $faker->numberBetween(0, 1);
            $user = new User();
            $user->setEmail($email);
            $user->setRoles($role);
            $user->setPassword($this->hasher->hashPassword($user, "password"));
            $user->setName($faker->lastName());
            $user->setFirstname($faker->firstName($genderTilte[$index]));
            $user->setBirthDate($faker->dateTimeBetween($startDate = '-30 years', $endDate = '2004-01-01', $timezone = null));
            // $user->setBirthDate($faker->dateTimeThisDecade($max ='2004-01-01', $timezone = null));
            $user->setGender($genderTilte[$index]);
            $user->setPhoneNumber($faker->mobileNumber());
            // $user->setPassword(
            //     $this->userPasswordEncoder->encodePassword($user, "azertyuiop")
            // );
            $manager->persist($user);
            $this->addReference("USER".$count, $user);
            $count++;
        }



        $manager->flush();
    }
}
