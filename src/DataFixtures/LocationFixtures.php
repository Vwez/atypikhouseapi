<?php

namespace App\DataFixtures;

use App\Entity\Location;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class LocationFixtures extends Fixture implements DependentFixtureInterface
{
    public function getDependencies(){
        return [
            CategoryFixtures::class,
            ThematiqueFixtures::class
        ];
    }

    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Faker\Factory::create("fr_FR");

        for ($i=0; $i < 30; $i++) { 
            $roomPersonne = rand(1, 5);
            $location = new Location();
            $location->setLibelle($faker->word());
            $location->setTitle($faker->word());
            $location->setMaxPerson($roomPersonne);
            $location->setRoom($roomPersonne);
            $location->setMinPrice($roomPersonne * 15);
            $location->setDescription($faker->text(200));
            $location->setNumber(rand(1, 100));
            $location->setStreet($faker->streetName());
            $location->setAdditionalAdress($faker->secondaryAddress());
            $location->setCity($faker->city());
            $location->setPostalCode( intval($faker->postcode()));
            $location->setCountry($faker->country());
            // $location->setDateLocation($faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now', $timezone = null));
            $location->setDateLocation($faker->dateTimeThisDecade($max ='2021-01-01', $timezone = null));
            $location->setCategory($this->getReference("CAT".mt_rand(0, 17)));
            $location->setThematiques($this->getReference("THE".mt_rand(0, 16)));
            $manager->persist($location);
            $this->addReference("LOC".$i, $location);

        }

        $manager->flush();
    }
}
