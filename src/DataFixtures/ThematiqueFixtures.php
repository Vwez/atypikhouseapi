<?php

namespace App\DataFixtures;

use App\Entity\Thematique;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ThematiqueFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $thematiques =  array(
            "romantique", "spa privatif", "familial", "retour à la natures","familial", "extraordinnaire",  
            "ecologique", "vélo", "animaux bienvenus","< 100 € ", "clampig", "luxe", "amateur de vin",
            "Pêche", "observatoire des étoiles", "montagne", "à la la ferme"
        );

        for ($i=0; $i <= count($thematiques) - 1 ; $i++) { 
            $thematique = new Thematique();
            $thematique->setLibelle($thematiques[$i]);
            $manager->persist($thematique);
            $this->addReference("THE".$i, $thematique);
         }

        $manager->flush();
    }
}
