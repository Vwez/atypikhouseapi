<?php

namespace App\DataFixtures;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Preference;
use Faker;

class PreferenceFixtures extends Fixture implements DependentFixtureInterface
{

    public function getDependencies(){
        return [
            UserFixtures::class,
        ];
    }

    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Faker\Factory::create("fr_FR");
        for ($i=0; $i < 5; $i++) { 
            $preference = new Preference();
            $preference->setChoice1($faker->word());
            $preference->setChoice2($faker->word());
            $preference->setChoice3($faker->word());
            $preference->setUser($this->getReference("USER".mt_rand(0, 3)));
            $manager->persist($preference );
            $this->addReference("PRE".$i, $preference);

        }

        $manager->flush();
    }
}
