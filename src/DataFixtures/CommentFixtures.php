<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Faker;

class CommentFixtures extends Fixture implements DependentFixtureInterface
{

    public function getDependencies(){
        return [
            ReservationFixtures::class,
            UserFixtures::class
        ];
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create("fr_FR");
        for ($i=0; $i < 30; $i++) { 
            $comment = new Comment();
            $comment->setTitle($faker->text(50));
            $comment->setContent($faker->text(160));
            $comment->setRating(rand(1,5));
            $comment->setNbrOfResponse(rand(1,10));
            $comment->setReservation($this->getReference("RES".mt_rand(0, 19)));
            $manager->persist($comment);
            $this->addReference("COM".$i, $comment);
            # code...
        }

        $manager->flush();
    }
}
