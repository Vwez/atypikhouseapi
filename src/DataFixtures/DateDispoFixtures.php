<?php

namespace App\DataFixtures;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\DateDispo;
// use App\Entity\Location;
use Faker;

class DateDispoFixtures extends Fixture implements DependentFixtureInterface
{

    public function getDependencies(){
        return [
            LocationFixtures::class,
        ];
    }

    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        $season = array("Hiver","automne","Printemps","été");
        $faker = Faker\Factory::create("fr_FR");
        for ($i=0; $i < 20; $i++) { 
            $index = rand(0, 3);
            $dateDispo = new DateDispo();
            $dateDispo->setSeason($season[$index]);
            $dateDispo->setDateStart($faker->dateTimeThisDecade($max ='2021-01-01', $timezone = null));
            $dateDispo->setDateEnd($faker->dateTimeThisDecade($max ='2021-01-01', $timezone = null));
            $dateDispo->setLocation($this->getReference("LOC".$i));
            $manager->persist($dateDispo);
            $this->addReference("DATD".$i, $dateDispo);
        }

        $manager->flush();
    }
}
