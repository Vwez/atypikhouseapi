<?php

namespace App\DataFixtures;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Value;
use Faker;

class ValueFixtures extends Fixture implements DependentFixtureInterface
{

    public function getDependencies(){
        return [
            PropertyFixturesPhp::class,
        ];
    }

    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Faker\Factory::create("fr_FR");
        for ($i=0; $i < 10; $i++) { 
            $value = new Value();
            $value->setLibelle($faker->word());
            $value->setProperty($this->getReference("PRO".mt_rand(0, 9)));
            $manager->persist($value);
            $this->addReference("VAL".$i, $value);
        }

        $manager->flush();
    }
}
