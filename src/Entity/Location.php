<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\LocationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LocationRepository::class)]
#[ApiResource]
class Location
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $libelle;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $title;

    #[ORM\Column(type: 'integer')]
    private $maxPerson;

    #[ORM\Column(type: 'integer')]
    private $room;

    #[ORM\Column(type: 'float', length: 255)]
    private $minPrice;

    #[ORM\Column(type: 'text', nullable: true)]
    private $description;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $number;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $street;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $additionalAdress;

    #[ORM\Column(type: 'string', length: 255)]
    private $city;

    #[ORM\Column(type: 'integer')]
    private $postalCode;

    #[ORM\Column(type: 'string', length: 255)]
    private $country;

    #[ORM\Column(type: 'date', nullable: true)]
    private $dateLocation;

    #[ORM\ManyToOne(targetEntity: Category::class, inversedBy: 'locations')]
    #[ORM\JoinColumn(nullable: false)]
    private $category;

    #[ORM\OneToMany(mappedBy: 'location', targetEntity: DateDispo::class)]
    private $dateDispos;

    #[ORM\ManyToOne(targetEntity: Thematique::class, inversedBy: 'location')]
    private $thematiques;

    #[ORM\OneToMany(mappedBy: 'location', targetEntity: Reservation::class)]
    private $reservations;

    

    public function __construct()
    {
        $this->dateDispos = new ArrayCollection();
        $this->reservations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getMaxPerson(): ?int
    {
        return $this->maxPerson;
    }

    public function setMaxPerson(int $maxPerson): self
    {
        $this->maxPerson = $maxPerson;

        return $this;
    }

    public function getRoom(): ?int
    {
        return $this->room;
    }

    public function setRoom(int $room): self
    {
        $this->room = $room;

        return $this;
    }

    public function getMinPrice(): ?string
    {
        return $this->minPrice;
    }

    public function setMinPrice(string $minPrice): self
    {
        $this->minPrice = $minPrice;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(?int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getAdditionalAdress(): ?string
    {
        return $this->additionalAdress;
    }

    public function setAdditionalAdress(?string $additionalAdress): self
    {
        $this->additionalAdress = $additionalAdress;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPostalCode(): ?int
    {
        return $this->postalCode;
    }

    public function setPostalCode(int $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getDateLocation(): ?\DateTimeInterface
    {
        return $this->dateLocation;
    }

    public function setDateLocation(?\DateTimeInterface $dateLocation): self
    {
        $this->dateLocation = $dateLocation;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection<int, DateDispo>
     */
    public function getDateDispos(): Collection
    {
        return $this->dateDispos;
    }

    public function addDateDispo(DateDispo $dateDispo): self
    {
        if (!$this->dateDispos->contains($dateDispo)) {
            $this->dateDispos[] = $dateDispo;
            $dateDispo->setLocation($this);
        }

        return $this;
    }

    public function removeDateDispo(DateDispo $dateDispo): self
    {
        if ($this->dateDispos->removeElement($dateDispo)) {
            // set the owning side to null (unless already changed)
            if ($dateDispo->getLocation() === $this) {
                $dateDispo->setLocation(null);
            }
        }

        return $this;
    }

    public function getThematiques(): ?Thematique
    {
        return $this->thematiques;
    }

    public function setThematiques(?Thematique $thematiques): self
    {
        $this->thematiques = $thematiques;

        return $this;
    }

    /**
     * @return Collection<int, Reservation>
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations[] = $reservation;
            $reservation->setLocation($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): self
    {
        if ($this->reservations->removeElement($reservation)) {
            // set the owning side to null (unless already changed)
            if ($reservation->getLocation() === $this) {
                $reservation->setLocation(null);
            }
        }

        return $this;
    }

    

    

    
}
