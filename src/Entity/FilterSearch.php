<?php

namespace App\Entity;

use App\Repository\FilterSearchRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: FilterSearchRepository::class)]
class FilterSearch
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Assert\Length(min:1, max:255)]
    #[Assert\Regex(pattern: "/^([A-Za-z0-9À-ÿ ',:\.-]+)$/")]
    #[Assert\NotBlank()]
    private $city;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Assert\Length(min:4, max:255)]
    #[Assert\Regex(pattern: "/^([A-Za-z0-9À-ÿ ',:\.-]+)$/" )]
    #[Assert\NotBlank()]
    private $category;

    #[ORM\Column(type: 'integer')]
    #[Assert\Regex(pattern: "/^([0-9])$/" )]
    private $nbPerson;

    #[ORM\Column(type: 'date')]
    #[Assert\NotNull]
    #[Assert\Type(\DateTimeInterface::class)]
    private $date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(?string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getNbPerson(): ?int
    {
        return $this->nbPerson;
    }

    public function setNbPerson(int $nbPerson): self
    {
        $this->nbPerson = $nbPerson;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
}
