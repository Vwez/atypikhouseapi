<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MessageRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: MessageRepository::class)]
#[ApiResource]
class Message
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Regex(pattern: "/^([A-Za-z0-9À-ÿ ',:\.-]+)$/", )]
    #[Assert\Length(min:1, max:255)]
    #[Assert\NotBlank()]
    private $name;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Assert\Regex(pattern: "/^([A-Za-z0-9À-ÿ ',:\.-]+)$/", )]
    #[Assert\Length(max:255)]
    private $firstName;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Regex(pattern: "/^([A-Za-z0-9À-ÿ ',:\.-]+)$/", )]
    #[Assert\Length(min:5,max:255)]
    #[Assert\NotBlank()]
    private $subject;

    #[ORM\Column(type: 'text')]
    #[Assert\Length(min:1)]
    #[Assert\NotBlank()]
    private $content;

    #[ORM\Column(type: 'string', length: 70)]
    #[Assert\Regex(pattern: "/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/", )]
    #[Assert\Length(min:6, max:70)]
    #[Assert\NotBlank()]
    private $forwarderMail;

    #[ORM\Column(type: 'datetime_immutable')]
    private $createAt;

    #[ORM\Column(type: 'time')]
    private $timeCreate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getForwarderMail(): ?string
    {
        return $this->forwarderMail;
    }

    public function setForwarderMail(string $forwarderMail): self
    {
        $this->forwarderMail = $forwarderMail;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeImmutable
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeImmutable $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getTimeCreate(): ?\DateTimeInterface
    {
        return $this->timeCreate;
    }

    public function setTimeCreate(\DateTimeInterface $timeCreate): self
    {
        $this->timeCreate = $timeCreate;

        return $this;
    }
}
