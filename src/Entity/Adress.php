<?php

namespace App\Entity;

use App\Repository\AdressRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: AdressRepository::class)]
class Adress
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Assert\Length(min:4, max:255)]
    #[Assert\Regex(pattern: "/^([A-Za-z0-9À-ÿ ',:\.-]+)$/", )]
    #[Assert\NotBlank()]
    #[ORM\Column(type: 'string', length: 255)]
    private $street;

    #[ORM\Column(type: 'integer')]
    private $streerNumber;

    #[Assert\Length(min:4, max:255)]
    #[Assert\Regex(pattern: "/^([A-Za-z0-9À-ÿ ',:\.-]+)$/", )]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $city;

    #[Assert\Length(min:4, max:255)]
    #[Assert\Regex(pattern: "/^([A-Za-z0-9À-ÿ ',:\.-]+)$/", )]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $country;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $postalCode;

    #[Assert\Length(min:4, max:255)]
    #[Assert\Regex(pattern: "/^([A-Za-z0-9À-ÿ ',:\.-]+)$/", )]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $additionalAdress;

    #[ORM\OneToOne(targetEntity: Location::class, cascade: ['persist', 'remove'])]
    private $location;

    #[ORM\OneToOne(targetEntity: User::class, cascade: ['persist', 'remove'])]
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getStreerNumber(): ?int
    {
        return $this->streerNumber;
    }

    public function setStreerNumber(int $streerNumber): self
    {
        $this->streerNumber = $streerNumber;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getPostalCode(): ?int
    {
        return $this->postalCode;
    }

    public function setPostalCode(?int $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getAdditionalAdress(): ?string
    {
        return $this->additionalAdress;
    }

    public function setAdditionalAdress(?string $additionalAdress): self
    {
        $this->additionalAdress = $additionalAdress;

        return $this;
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function setLocation(?Location $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

}