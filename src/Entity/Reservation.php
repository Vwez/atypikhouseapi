<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ReservationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ReservationRepository::class)]
#[ApiResource]
class Reservation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'float')]
    private $TtcPrice;

    #[ORM\Column(type: 'date')]
    private $bookingStartDate;

    #[ORM\Column(type: 'date')]
    private $bookingEndDate;

    #[ORM\Column(type: 'date')]
    private $bookingDate;

    #[ORM\ManyToOne(targetEntity: Location::class, inversedBy: 'reservations')]
    #[ORM\JoinColumn(nullable: false)]
    private $location;

    #[ORM\OneToMany(mappedBy: 'reservation', targetEntity: Comment::class)]
    private $comments;

    #[ORM\Column(type: 'string', length: 255)]
    private $reservationState;

    #[ORM\ManyToOne(targetEntity: User::class)]
    private $user;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTtcPrice(): ?float
    {
        return $this->TtcPrice;
    }

    public function setTtcPrice(float $TtcPrice): self
    {
        $this->TtcPrice = $TtcPrice;

        return $this;
    }

    

    public function getBookingStartDate(): ?\DateTimeInterface
    {
        return $this->bookingStartDate;
    }

    public function setBookingStartDate(\DateTimeInterface $bookingStartDate): self
    {
        $this->bookingStartDate = $bookingStartDate;

        return $this;
    }

    public function getBookingEndDate(): ?\DateTimeInterface
    {
        return $this->bookingEndDate;
    }

    public function setBookingEndDate(\DateTimeInterface $bookingEndDate): self
    {
        $this->bookingEndDate = $bookingEndDate;

        return $this;
    }

    public function getBookingDate(): ?\DateTimeInterface
    {
        return $this->bookingDate;
    }

    public function setBookingDate(\DateTimeInterface $bookingDate): self
    {
        $this->bookingDate = $bookingDate;

        return $this;
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function setLocation(?Location $location): self
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return Collection<int, Comment>
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setReservation($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getReservation() === $this) {
                $comment->setReservation(null);
            }
        }

        return $this;
    }

    public function getReservationState(): ?string
    {
        return $this->reservationState;
    }

    public function setReservationState(string $reservationState): self
    {
        $this->reservationState = $reservationState;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
